import bcrypt from 'bcrypt'
import express from 'express'

import User, { IUser } from '../models/user'


const router = express.Router()

router.get('/', (req, res, next) => {
  User.find({})
    .populate('notes', {
      content: 1,
      date: 1,
    })
    .then(users => res.json(users))
    .catch(next)
})

router.post('/', (req, res, next) =>
  bcrypt.hash(req.body.password, 10)
    .then(hash =>
      new User({
        username: req.body.username,
        name: req.body.name,
        passwordHash: hash
      } as IUser).save())
    .then(user =>
      res.json(user))
    .catch(next)
)


export default router

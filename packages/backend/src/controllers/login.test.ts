import bcrypt from 'bcrypt'
import dotenv from 'dotenv'
import mongoose from 'mongoose'
import supertest from 'supertest'
import { MongoMemoryServer } from 'mongodb-memory-server'

import app from '../app'
import User, { IUser } from '../models/user'


dotenv.config()

const api = supertest(app)
const mongoServer = new MongoMemoryServer()

beforeAll(async () => {
  const uri = await mongoServer.getUri()
  await mongoose.connect(uri, {
    autoCreate: true,
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
})

afterAll(async () => {
  await mongoose.disconnect()
  await mongoServer.stop()
})

beforeEach(async () => {
  await User.deleteMany({})
})

describe("POST /api/login", () => {
  it("should accept valid credentials and return a token", async () => {
    await new User({
      username: 'newuser',
      passwordHash: await bcrypt.hash('password', 1)
    } as IUser).save()
    await api
      .post('/api/login')
      .send({
        username: 'newuser',
        password: 'password',
      })
      .expect(200)
      .expect('Content-Type', /application\/json/i)
      .expect(res => {
        expect(res.body).toMatchObject({
          token: expect.anything()
        })
      })
  })

  it("should reject invalid credentials", async () => {
    await new User({
      username: 'newuser',
      passwordHash: await bcrypt.hash('password', 1)
    } as IUser).save()
    await api
      .post('/api/login')
      .send({
        username: 'newuser',
        password: 'badpassword',
      })
      .expect(401)
      .expect('Content-Type', /application\/json/i)
      .expect(res => {
        expect(res.body).toMatchObject({
          error: {
            message: expect.stringMatching(/invalid username or password/i),
            status: 401,
          }
        })
      })
  })

  it("should reject empty credentials", async () => {
    await api
      .post('/api/login')
      .send({})
      .expect(401)
      .expect('Content-Type', /application\/json/i)
      .expect(res => {
        expect(res.body).toMatchObject({
          error: {
            message: expect.stringMatching(/invalid username or password/i),
            status: 401,
          }
        })
      })
  })


})

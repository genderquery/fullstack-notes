import bcrypt from 'bcrypt'
import express from 'express'
import jwt from 'jsonwebtoken'
import { Unauthorized } from 'http-errors'

import User from '../models/user'


const router = express.Router()

router.post('/', (req, res, next) => {
  Promise.resolve()
    .then(() => User.findOne({ username: req.body.username }))
    .then(user => {
      if (!user) {
        throw new Unauthorized("Invalid username or password")
      }
      return user
    }).then(user =>
      Promise.resolve()
        .then(() => bcrypt.compare(req.body.password || '', user?.passwordHash || ''))
        .then(isPasswordCorrect => {
          if (!isPasswordCorrect) {
            throw new Unauthorized("Invalid username or password")
          }
        })
        .then(() => {
          const token = jwt.sign({
            username: user.username,
            id: user.id,
          }, process.env.JWT_SECRET!)
          res.json({
            username: user.username,
            token,
          })
        })
    ).catch(next)
})

export default router

import express, { Request } from 'express'
import jwt from 'jsonwebtoken'
import { BadRequest, NotFound, Unauthorized } from 'http-errors'
import status from 'statuses'

import Note, { INote } from '../models/note'
import User from '../models/user'


const router = express.Router()

const getTokenFrom = (request: Request): Promise<any> =>
  Promise.resolve()
    .then(() => {
      const auth = request.get('Authorization')
      const match = auth?.match(/bearer\s+(\S+)/i)
      const token = match && match[1]
      if (token) {
        return jwt.verify(token, process.env.JWT_SECRET!)
      }
      throw new Unauthorized(`token missing or invalid`)
    })

const getUserFrom = (request: Request) =>
  getTokenFrom(request)
    .then(token => User.findById(token.id))
    .then(user => {
      if (!user) {
        throw new Unauthorized(`token missing or invalid`)
      }
      return user
    })


router.get('/', (req, res, next) =>
  getUserFrom(req)
    .then(user => Note.find({ user: user.id }))
    .then(notes => res.json(notes))
    .catch(next)
)

router.get('/:id', (req, res, next) =>
  getUserFrom(req)
    .then(user => Note.findOne({ user: user.id, _id: req.params.id }))
    .then(note => {
      if (note) {
        res.json(note)
      } else {
        throw new NotFound(`No note with id "${req.params.id}"`)
      }
    })
    .catch(next)
)

router.post('/', (req, res, next) =>
  getUserFrom(req)
    .then(user => new Note({
      content: req.body.content,
      important: req.body.important || false,
      date: new Date(),
      user: user
    }).save()
      .then(note => {
        user.notes = user.notes!.concat(note.id)
        user.save()
          .then(() => res.json(note))
      }))
    .catch(next)
)

router.delete('/:id', (req, res, next) =>
  getUserFrom(req)
    .then(user => Note.deleteOne({ user: user.id, _id: req.params.id }))
    .then(() => res.status(status('No Content')).end())
    .catch(next)
)

router.put('/:id', (req, res, next) =>
  getUserFrom(req)
    .then(user =>
      Note.findOneAndUpdate({ user: user.id, _id: req.params.id }, {
        content: req.body.content,
        important: req.body.important,
      }))
    .then(note => res.json(note))
    .catch(next)
)


export default router

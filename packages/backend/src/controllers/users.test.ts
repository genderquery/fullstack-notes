import dotenv from 'dotenv'
import mongoose from 'mongoose'
import { MongoMemoryServer } from 'mongodb-memory-server'
import supertest from 'supertest'

import app from '../app'
import User, { IUser } from '../models/user'

dotenv.config()

const api = supertest(app)
const mongoServer = new MongoMemoryServer()

beforeAll(async () => {
  const uri = await mongoServer.getUri()
  await mongoose.connect(uri, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
})

afterAll(async () => {
  await mongoose.disconnect()
  await mongoServer.stop()
})

beforeEach(async () => {
  await User.deleteMany({})
})

describe("POST /api/users", () => {
  it("should create user with valid credentials", async () => {
    await api
      .post('/api/users')
      .send({
        username: 'newuser',
        password: 'password',
      })
      .expect(200)
      .expect('Content-Type', /application\/json/i)
      .expect(res =>
        expect(res.body).toMatchObject({
          username: 'newuser',
        }))
  })

  it("should reject duplicate usernames", async () => {
    await api
      .post('/api/users')
      .send({
        username: 'newuser',
        password: 'password',
      })
      .expect(200)
      .expect('Content-Type', /application\/json/i)
      .expect(res =>
        expect(res.body).toMatchObject({
          username: 'newuser',
        }))

    await api
      .post('/api/users')
      .send({
        username: 'newuser',
        password: 'password',
      })
      .expect(400)
      .expect('Content-Type', /application\/json/i)
      .expect(res =>
        expect(res.body).toMatchObject({
          error: {
            message: expect.stringMatching(/expected `username` to be unique/i),
            status: 400,
          }
        })
      )
  })
})

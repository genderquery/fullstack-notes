import dotenv from 'dotenv'
import mongoose from 'mongoose'
import { MongoMemoryServer } from 'mongodb-memory-server'
import supertest from 'supertest'
import jwt from 'jsonwebtoken'

import app from '../app'
import Note from '../models/note'
import User, { IUser } from '../models/user'

dotenv.config()

const api = supertest(app)
const mongoServer = new MongoMemoryServer()

let token: string
let user: IUser

beforeAll(async () => {
  const uri = await mongoServer.getUri()
  await mongoose.connect(uri, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  user = await new User({
    username: 'testuser'
  }).save()
  token = jwt.sign({
    username: user.username,
    id: user.id,
  }, process.env.JWT_SECRET!)
})

afterAll(async () => {
  await mongoose.disconnect()
  await mongoServer.stop()
})

beforeEach(async () => {
  await Note.deleteMany({})
})

describe("GET /api/notes", () => {
  it("should reject unauthorized access", async () =>
    await api
      .get('/api/notes')
      .expect(401)
  )

  it("should return an empty list when there are no notes", async () => {
    const res = await api
      .get('/api/notes')
      .set('Authorization', `Bearer ${token}`)
      .expect(200)
      .expect('Content-Type', /application\/json/)
    expect(res.body).toEqual([])
  })

  it("should return a note that was inserted", async () => {
    const note = await new Note({
      content: "new note",
      user: user.id,
    }).save()
    const res = await api
      .get('/api/notes')
      .set('Authorization', `Bearer ${token}`)
      .expect(200)
      .expect('Content-Type', /application\/json/)
    expect(res.body.length).toBe(1)
    expect(res.body[0]).toMatchObject({
      content: "new note"
    })
  })
})

describe("GET /api/notes/:id", () => {
  it("should reject unauthorized access", async () => {
    const note = await new Note({
      content: "new note",
      user: user.id,
    }).save()
    await api
      .get(`/api/notes/${note.id}`)
      .expect(401)
  })

  it("should return not accept invalid ids", async () => {
    const res = await api
      .get(`/api/notes/invalid`)
      .set('Authorization', `Bearer ${token}`)
      .expect(400)
      .expect('Content-Type', /application\/json/)
    expect(res.body).toMatchObject({
      error: {
        message: expect.stringMatching(/Cast to ObjectId failed/i),
        status: 400,
      }
    })
  })

  it("should return not found for non-existent notes", async () => {
    const res = await api
      .get(`/api/notes/000000000000`)
      .set('Authorization', `Bearer ${token}`)
      .expect(404)
      .expect('Content-Type', /application\/json/i)
    expect(res.body).toMatchObject({
      error: {
        message: expect.stringMatching(/no note with id/i),
        status: 404,
      }
    })
  })

  it("should return a note with a valid id", async () => {
    const note = await new Note({
      content: "new note",
      user: user.id,
    }).save()
    const res = await api
      .get(`/api/notes/${note.id}`)
      .set('Authorization', `Bearer ${token}`)
      .expect(200)
      .expect('Content-Type', /application\/json/i)
    expect(res.body).toMatchObject({
      content: "new note"
    })
  })
})

describe("POST /api/notes", () => {
  it("should accept a valid note", async () => {
    await api
      .post('/api/notes')
      .set('Authorization', `Bearer ${token}`)
      .send({
        content: "new note",
        user: user.id,
      })
      .expect(200)
      .expect('Content-Type', /application\/json/i)
      .expect(res => {
        expect(res.body).toMatchObject({
          content: "new note"
        })
      })
  })
  it("should reject an invalid note", async () => {
    await api
      .post('/api/notes')
      .set('Authorization', `Bearer ${token}`)
      .send({
        text: "new note",
        user: user.id,
      })
      .expect(400)
      .expect('Content-Type', /application\/json/i)
      .expect(res => {
        expect(res.body).toMatchObject({
          error: {
            message: expect.stringMatching(/validation failed/i),
            status: 400,
          }
        })
      })
  })
})

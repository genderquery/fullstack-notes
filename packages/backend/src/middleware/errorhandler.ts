import { ErrorRequestHandler } from 'express'
import mongoose from 'mongoose'
import { HttpError } from 'http-errors'
import status from 'statuses'

const errorhandler: ErrorRequestHandler = (err, req, res, next) => {
  if (err instanceof HttpError) {
    res.status(err.status).json({
      error: {
        status: err.status,
        message: err.message,
      }
    })
  } else if (err instanceof mongoose.Error.CastError || mongoose.Error.ValidationError) {
    const statusCode = status('Bad Request')
    res.status(status('Bad Request')).json({
      error: {
        status: statusCode,
        message: err.message,
      }
    })
  } else {
    console.error(err)
    next(err)
  }
}

export default errorhandler

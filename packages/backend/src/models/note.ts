import mongoose from 'mongoose'
import { IUser } from './user'


export interface INote {
  [field: string]: unknown,
  content: string,
  date?: Date,
  important?: boolean,
  user: IUser,
}

const schema = new mongoose.Schema({
  content: {
    type: String,
    required: true,
    minlength: 5,
  },
  date: Date,
  important: Boolean,
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
  },
})

schema.set('toJSON', {
  transform: (doc, ret) => {
    ret.id = ret._id.toString()
    delete ret._id
    delete ret.__v
  }
})

const model = mongoose.model<INote & mongoose.Document>('Note', schema)


export default model

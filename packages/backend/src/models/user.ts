import mongoose from 'mongoose'
import uniqueValidator from 'mongoose-unique-validator'

import { INote } from './note'


export interface IUser {
  [field: string]: unknown,
  username: string,
  name?: string,
  passwordHash?: string,
  notes?: INote[],
}

const schema = new mongoose.Schema({
  username: {
    type: String,
    required: true,
    unique: true,
  },
  name: String,
  passwordHash: String,
  notes: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Note'
    }
  ]
})

schema.plugin(uniqueValidator)

schema.set('toJSON', {
  transform: (doc, ret) => {
    ret.id = ret._id.toString()
    delete ret._id
    delete ret.__v
    delete ret.passwordHash
  }
})

const model = mongoose.model<IUser & mongoose.Document>('User', schema)


export default model

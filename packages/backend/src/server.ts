import dotenv from 'dotenv'
import http from 'http'
import mongoose from 'mongoose'

import app from './app'

const DEBUG = process.env.NODE_ENV === 'development'

dotenv.config({
  debug: DEBUG
})

if (!process.env.MONGODB_URI) {
  throw Error("MONGODB_URI must be defined")
}

mongoose.connect(process.env.MONGODB_URI, {
  autoCreate: DEBUG,
  autoIndex: DEBUG,
  useNewUrlParser: true,
  useUnifiedTopology: true,
}).then(db => {
  console.log(`Connected to ${process.env.MONGODB_URI}`)

  const port = process.env.PORT
  const host = process.env.HOST
  const server = http.createServer(app)
  server.listen({ port, host }, () => {
    console.log(`Listening at http://${host}:${port}`)
  })
})

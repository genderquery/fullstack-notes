import cors from 'cors'
import express from 'express'
import helmet from 'helmet'
import morgan from 'morgan'

import loginRouter from './controllers/login'
import notesRouter from './controllers/notes'
import usersRouter from './controllers/users'
import errorhandler from './middleware/errorhandler'


const app = express()

app.use(morgan('dev'))
app.use(cors())
app.use(helmet())
app.use(express.json())
app.use('/api/login', loginRouter)
app.use('/api/notes', notesRouter)
app.use('/api/users', usersRouter)
app.use(errorhandler)


export default app
